#include <stdint.h>
//
// Created by Lisi4ka on 29.10.2023.
//



#ifndef IMAGE_TRANSFORMER_IMAGE_STRUCT_H
#define IMAGE_TRANSFORMER_IMAGE_STRUCT_H

struct pixel {
    uint8_t components[3];
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};


#endif //IMAGE_TRANSFORMER_IMAGE_STRUCT_H
