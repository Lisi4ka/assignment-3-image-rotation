//
// Created by Lisi4ka on 29.10.2023.
//
#include "bmp_io.h"
#include <stdlib.h>


enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header input_header;
    fread(&input_header, sizeof(input_header), 1,  in);
    (*img).height = input_header.biHeight;
    img->width = input_header.biWidth;
    if (input_header.bfType != 19778) {
        return READ_INVALID_SIGNATURE;
    }
    if (input_header.bOffBits != 54) {
        return READ_INVALID_HEADER;
    }
    if (input_header.biBitCount != 24) {
        return READ_INVALID_BITS;
    }

    uint64_t real_width = (4 - ((img->width)*3)%4)+(img->width*3);
    if (((img->width)*3)%4 == 0) {
        real_width -= 4;
    }
    char *data = malloc(real_width * img->height);
    char *copy_data = malloc(img->width * img->height * 3);
    if (data == NULL || copy_data == NULL) {
        free(data);
        free(copy_data);
        return MEMORY_ERROR;
    }
    fseek(in, sizeof(input_header), SEEK_SET);
    fread(data, real_width*img->height,1, in);
    int count = 0;
    for (int i = 0; i < img->height; i++) {
        for (int j =0; j < real_width; j++) {
            if (j < img->width*3) {
                copy_data[count] = data[j+i*real_width];
                count++;
            }
        }
    }

    free(data);

    img->data = (struct pixel *) copy_data;
    /*
    printf("\n%i %i\n", input_header.biHeight, input_header.biWidth);
    printf("input_header.bfType = %d;\n", input_header.bfType);
    printf("input_header.bfileSize = %d;\n", input_header.bfileSize);
    printf("input_header.bfReserved = %d;\n", input_header.bfReserved);
    printf("input_header.bOffBits = %d;\n", input_header.bOffBits);
    printf("input_header.biSize = %d;\n", input_header.biSize);
    printf("input_header.biWidth = %d;\n", input_header.biWidth);
    printf("input_header.biHeight = %d;\n", input_header.biHeight);
    printf("input_header.biPlanes = %d;\n", input_header.biPlanes);
    printf("input_header.biBitCount = %d;\n", input_header.biBitCount);
    printf("input_header.biCompression = %d;\n", input_header.biCompression);
    printf("input_header.biSizeImage = %d;\n", input_header.biSizeImage);
    printf("input_header.biXPelsPerMeter = %d;\n", input_header.biXPelsPerMeter);
    printf("input_header.biYPelsPerMeter = %d;\n", input_header.biYPelsPerMeter);
    printf("input_header.biClrUsed = %d;\n", input_header.biClrUsed);
    printf("input_header.biClrImportant = %d;\n", input_header.biClrImportant);
    */

    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    uint64_t garbage = 4 - ((img->width)*3)%4;
    uint64_t count1 = 0;
    uint8_t *copy_data = malloc((img->width * 3 + garbage) * (img->height + 3));
    if (copy_data == NULL) {
        free(copy_data);
        return MEMORY_ALLOCATION_ERROR;
    }
    for (int i = 0; i < img->height; i++) {
        for (int w = 0; w < img->width; w++) {
            copy_data[count1] = img->data[i*img->width + w].components[0];
            count1++;
            copy_data[count1] = img->data[i*img->width + w].components[1];
            count1++;
            copy_data[count1] = img->data[i*img->width + w].components[2];
            count1++;
        }
        for (int j = 0; j < garbage; j++) {
            copy_data[count1] = 0;
            count1++;
        }
    }
    printf("bmp alive");
    struct bmp_header input_header;
    input_header.biHeight = img->height;
    input_header.biWidth = img->width;
    input_header.bfType = 19778;
    input_header.bfileSize = count1 + 54;
    input_header.bfReserved = 0;
    input_header.bOffBits = 54;
    input_header.biSize = 40;
    input_header.biPlanes = 1;
    input_header.biBitCount = 24;
    input_header.biCompression = 0;
    input_header.biSizeImage = count1;
    input_header.biXPelsPerMeter = 2834;
    input_header.biYPelsPerMeter = 2834;
    input_header.biClrUsed = 0;
    input_header.biClrImportant = 0;

    if(fwrite(&input_header, sizeof(input_header), 1, out) != 1) {
        free(copy_data);
        return WRITE_ERROR;
    }
    size_t pr = fwrite(copy_data, count1, 1, out);
    if (pr != 1) {
        free(copy_data);
        return WRITE_ERROR;
    }
    free(copy_data);
    printf("still live");
    return WRITE_OK;
}
