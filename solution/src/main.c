#include "bmp_io.h"
#include "image_rotate.h"
#include "image_struct.h"
#include <stdio.h>
#include <stdlib.h>


int main( int argc, char** argv ) {
    (void) argc;
    (void) argv; // suppress 'unused parameters' warning
    long count = strtol(argv[3], 0, 10) < 0 ? (((360 + strtol(argv[3], 0, 10)) / 90)
            == 3) ? 1 : (((360 + strtol(argv[3], 0, 10)) / 90) == 1) ? 3 : ((360 +
            strtol(argv[3], 0, 10)) / 90) : (strtol(argv[3], 0, 10) / 90
            == 1) ? 3 : (strtol(argv[3], 0, 10) / 90 == 3)
            ? 1 :strtol(argv[3], 0, 10) / 90;
    if (count > 3 || count < 0) {
        printf("Угол %s не поддерживается!", argv[3]);
        return 0;
    }
    FILE *input = fopen(argv[1], "rb");
    FILE *output = fopen(argv[2], "wb");
    struct image img;
    from_bmp(input, &img);
    for (int i = 0; i < count; i++) {
        img = rotate(img);
    }
    to_bmp(output, &img);
    free(img.data);
    fclose(output);
    return 0;
}
