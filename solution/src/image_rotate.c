//
// Created by Lisi4ka on 31.10.2023.
//
#include "image_rotate.h"
#include "image_struct.h"
#include <stdlib.h>
#include <string.h>


struct image rotate( struct image const source ) {
    struct pixel* data = malloc(source.height*source.width*3);
    if (data == NULL) {
        struct image invalidImage;
        invalidImage.width = 0;
        invalidImage.height = 0;
        invalidImage.data = NULL;
        free(data);
        return invalidImage;
    }
    int count = 0;
    for (int i = 0; i < source.width; i++) {
        for (int j = 0; j < source.height; j++) {
            data[count] = source.data[j*source.width+i];
            count++;
        }
    }


    struct image new_image;
    struct pixel c;
    new_image.width = source.height;
    new_image.height = source.width;
    for (int i = 0; i < new_image.height; i++) {
        for (int j = 0; j < new_image.width / 2; j++) {
            c = data[i*new_image.width+(new_image.width - j - 1)];
            data[i*new_image.width+(new_image.width - j - 1)] = data[i*new_image.width+j];
            data[i*new_image.width+j] = c;
        }
    }
    for (int i = 0; i < new_image.width * new_image.height; i++) {
        source.data[i] = data[i];
    }
    free(data);
    new_image.data = source.data;
    return new_image;
}
